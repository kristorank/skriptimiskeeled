#!/usr/bin/python
# -*- coding: utf-8 -*-
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled arvestus

# Viited:
# 1 - Pärit enda koodist py-kodutoo.py

# Väljumiskoodid:
# 1 - Vale arv parameetreid
# 2 - Sisendfaili ei saanud avada
# 3 - Tekkis üldine viga URLi pärimisel või sõne otsimisel

import sys  # Moodul parameetrite(args) lugemiseks
import urllib2  # Moodul URL'ide pärimiseks
import time # Moodul kellaaja saamiseks

indeks = 0

# Kontrollime, kas skriptile antakse õige arv parameetreid (Viide 1)
if len(sys.argv) == 3:  # Parameetreid on kokku kolm, skript ise kaasa arvatud
    url = sys.argv[1]
    print "URL: " + url
    sisendf = sys.argv[2]
    print "Sisendfail: " + sisendf

else:
    print "Vale arv parameetreid!"
    print "Kasutamine: " + sys.argv[0] + " <Server/IP> <väljundfail>"
    exit(1)


# Kontrollime, kas ette antud aadressil on http ja või www olemas
if "http://" not in url:
	if "www" not in url:
		url = "http://www." + url
	else:
		url = "http://" + url

# Avame sisendfaili ja loeme read massiivi
try:
	with open(sisendf) as f:
		massiiv = [line.rstrip().split(',') for line in f]
		
except IOError:
	print "Faili " + sisendf + " avamisel tekkis viga!"
	exit(2)

# Väljundi loomine
for line in massiiv:

	# Liidame URL'i ja täpsustava osa
	otsiurl = url + massiiv[indeks][0]
	
	# Salvestame muutujasse otsitava sõne
	s6ne = massiiv[indeks][1]
	
	# Salvestame muutujasse praeguse kellaaja
	kellaaeg = time.strftime('%Y-%m-%d_%H-%M-%S')
	
	# Koostame väljundi
	valjund = otsiurl + "," + s6ne + "," + kellaaeg
	
	ok = ",OK"
	nok = ",NOK"
	
	# Pärime vastavat URL ja otsime sõnet
	try:
		p2ring = urllib2.urlopen(otsiurl).read()
		
		if str(p2ring.find(s6ne)) != "-1":
			print valjund + ok # Kui sõne leiti
			
		else:
			print valjund + nok # Kui sõnet ei leitud
			
	except urllib2.HTTPError:
		print valjund + nok
		continue
		
	except:
		print "Tekkis viga!"
		exit(3)
	
	indeks += 1
	
print "Skript lõpetas edukalt töö!"
