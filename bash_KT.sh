#!/bin/bash
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled Bash Kontrolltöö
# Skript v2ljastab pangakaartide PIN-koode

# Kontroll: Kas skript käivitati vajalike (juurkasutaja) õigustega
if [ $UID -ne 0 ]; then
    echo "Käivita skript $(basename $0) juurkasutaja õigustes (nt sudo ...)"
    exit 1
fi

# Kontroll: Kas skriptile on antud korrektne arv parameetreid
if [ $# -gt 1 ]; then
    echo "Kasutamine: $0 <PIN>"
    exit 1
fi

# Kontroll: yks neljakohaline parameeter
if [ $# -eq 1 ] && [ ${#1} -ne 4 ]; then
    echo "Kasutamine: $0 <neljakohaline PIN>"
    exit 1
fi

# Muutujate deklareerimine
PIN=$1
FAIL=koodid.txt
LIPP=0

# Tsykkel kirjutab v2lja arvud 0000 kuni 9999
for NUM in $( seq -w 0 9999 )
do
    # Kui anti yks parameeter
    if [ $# -ne 0 ]; then
        # Kui joutakse parameetrina antud arvuni
        if [ $PIN -eq $NUM ]; then
            LIPP=1 # Boolean, parameeteriga skripti jooksutamine
            if [ ! -d "$PIN" ]; then
                mkdir -p $PIN # Kausta kontroll ja vajadusel loomine
            elif [ ! -f "$PIN/koodid.txt" ]; then
                touch $PIN/koodid.txt # Faili kontroll ja vajadusel loomine
            else
             FAIL="$RANDOM$FAIL"
                touch $PIN/$FAIL # Kui fail olemas, siis teeb uue faili random algusega
            fi
        fi
    fi
    # Kui jooksutati parameetriga, siis v2ljastab faili ka
    if [ $LIPP -eq 1 ]; then
        echo $NUM >> $PIN/$FAIL
    fi
    echo $NUM # Arvude v2ljastamine
done


