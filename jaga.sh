#!/bin/bash
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled 1. kodutöö (bash)
# Skript loob grupile jagatud kausta

#Väljumiskoodid:
# 1 - puuduvad juurkasutaja õigused
# 2 - vale arv parameetreid
# 3 - viga repode uuendamisel või samba paigaldamisega
# 4 - samba konfi testparm ei olnud edukas
# 5 - kausta loomine ebaõnnestus

# v0.1 - Parameetrite arvu kontroll
# v0.2 - Kasutajaõiguste kontroll. Samba kontroll/paigaldamine. Grupi ja kausta kontroll/loomine. Samba konfi koopia tegemine.
# v0.3 - Samba konfi muutmine. Muudetud konfi testimine.
# v0.4 - Täispika tee kontroll. Viimistlemine.
# v0.5 - Lisakontroll olukorras, kus kausta tee algab punktiga.
# v0.6 - Täispikatee kontrolli parendamine. Kausta loomise lisakontroll.

# Kontroll: juurkasutaja õigused
if [ $UID -ne 0 ]; then
    echo "Käivita skript $(basename $0) juurikana"
    exit 1
fi

# Kontroll: õige arv muutujaid
if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <JAGATUD KAUST>"
    exit 2
fi

# Muutujate väärtustamine
KAUST=$1
GRUPP=$2
JAGAKAUST=${3-$(basename $KAUST)}

# Kontroll: Kausta täispikk tee, suhtelise tee korral asendatakse täispikaga
if [[ ${KAUST:0:1} != "/" ]]; then
    if [[ ${KAUST:0:2} == "./" ]]; then
        KAUST=${KAUST:1}
        KAUST="$(pwd)$KAUST"
    else
        KAUST="$(pwd)/$KAUST"
    fi    
fi

# Muutujate väärtuste kontroll. Välja kommenteeritud
:<<'KOMM'
echo "KAUST=$KAUST"
echo "GRUPP=$GRUPP"
echo "JAGAKAUST=$JAGAKAUST"
KOMM

# Kontroll: samba paigaldatud, kui ei, siis paigaldab
type smbd > /dev/null 2>&1

if [ $? -ne 0 ]; then
    echo "Sambat ei leitud. Paigaldame.."
    apt-get update > /dev/null 2>&1 && apt-get install samba -y || exit 3
else
    echo "Samba olemas."
fi

# Kontroll: kausta olemasolu, kui ei, siis loob
test -d $KAUST || mkdir -p $KAUST
if [ $? -ne 0 ]; then
    echo "Kausta loomisel tekkis viga."
    exit 5
fi

# Kontroll: grupi olemasolu, kui ei, siis loob
getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null

# samba konfist koopiate tegemine
cp /etc/samba/smb.conf /etc/samba/smb.conf.old
cp /etc/samba/smb.conf /etc/samba/smb.conf.test

# Samba konfis muudatuste sisseviimine (koopias)
cat >> /etc/samba/smb.conf.test << LOPP

[$JAGAKAUST]
comment=Jagatud kaust
path=$KAUST
writable=yes
valid users=@$GRUPP
force group=$GRUPP
browsable=yes
create mask=0664
directory mask=0775

LOPP

# Kontroll: Samba testkonf
testparm -s /etc/samba/smb.conf.test > /dev/null 2>&1

if [ $? -eq 0 ]; then
    echo "Samba konf OK."
   cp /etc/samba/smb.conf.test /etc/samba/smb.conf
else
   echo "Viga samba konfis"
   exit 4
fi

# Samba teenuse taaskäivitamine
echo "Taaskäivitan samba teenuse.."
service smbd reload

echo "Grupile '$GRUPP' jagatud kausta '$JAGAKAUST' loomine õnnestus!"
