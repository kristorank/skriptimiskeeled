#!/bin/bash
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled 2. kodutöö (bash)
# Skript loob kasutajale uue veebikodu

# Väljumiskoodid:
# 1 - Skript käivitati puudulike õigustega
# 2 - Skriptile anti vale arv parameetreid
# 3 - Apache2 paigaldamisel tekkis viga
# 4 - Veebikodu kausta ei saanud luua
# 5 - Konfi/lehe aktiveerimisel tekkis viga
# 6 - Apache serverit ei saanud käivitada
# 7 - Loodud lehe testimisel tekkis viga

# Versiooni ajalugu:
# v0.1 - Õiguste kontroll. Parameetrite kontroll. Apache paigaldamine/kontroll. Nimelahenduse lisamine /etc/hosts faili. Veebikodu kausta kontroll. Index faili kopeerimine. 
# v0.2 - Index faili sisu muutmine. Konfi kopeerimine ja konfis muudatuste sisse viimine. Lehe aktiveerimine. Apache taaskäivitus. Loodud lehekülje testimine.

# Kontroll: Kas skript käivitati vajalike (juurkasutaja) õigustega
if [ $UID -ne 0 ]; then
    echo "Käivita skript $(basename $0) juurkasutaja õigustes (nt sudo ...)"
    exit 1
fi

# Kontroll: Kas skriptile on antud korrektne arv parameetreid
if [ $# -ne 1 ]; then
    echo "Kasutamine: $0 <veebilehe nimi>"
    exit 2
fi

# Muutuja(te) väärtustamine
URL=$1 # lehekülje URL/URI
KODU=/var/www # Kodulehtede asukoht
APACHE=/etc/apache2/sites-available # Apache konfide asukoht

# Kontroll: Kas apache2 server on olemas, puudumisel paigaldatakse
which apache2 > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Apache2 serverit ei leitud. Paigaldame.."
    apt-get update && apt-get install apache2 -y || exit 3
else
    echo "Apache2 server olemas."
fi

# Kontroll: Kas veebikodu kaust on juba olemas
test -d $KODU/$URL || mkdir -p $KODU/$URL
if [ $? -ne 0 ]; then
    echo "Veebikodu kausta loomisel tekkis viga."
    exit 4
fi

# Nimelahenduse loomine (/etc/hosts failis)
grep "127.0.0.1 $URL" /etc/hosts > /dev/null 2>&1 || echo "127.0.0.1 $URL" >> /etc/hosts

# Kopeerime vaikimisi veebisaidi uude veebikodu kausta
cp $KODU/html/index.html $KODU/$URL

# Index.html faili sisu muutmine vastavalt loodud lehele
cat > $KODU/$URL/index.html << LOPP

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>$URL</title>
</head>
<body>
    <h1>Tere tulemast veebilehele $URL</h1>
</body>
</html>

LOPP

# Kopeerime vaikimisi veebisaidi konfi uue veebikodu konfiks
cp $APACHE/000-default.conf $APACHE/$URL.conf

# Veebikodu konfis muudatuste tegemine
sed -e "s;#ServerName www.example.com;ServerName $URL;" -e "s;DocumentRoot /var/www/html;DocumentRoot $KODU/$URL;" -i $APACHE/$URL.conf

# Loodud lehe aktiveerimine / konfi lingi loomine sites-enabled kausta
a2ensite $URL > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Veebikodu aktiveerimisel tekkis viga."
    exit 5
fi

# Apache serveri taaskäivitamine
service apache2 restart > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Apache taaskäivitamisel tekkis viga."
    exit 6
else
    echo "Apache server taaskäivitatud."
fi

# Veebikodu testimine
wget $URL/index.html -O /dev/null -q
if [ $? -ne 0 ]; then
    echo "Veebikodu testimisel tekkis viga."
    exit 7
else
    echo "Veebikodu test õnnestus."
fi

echo "Veebikodu $URL loomine oli edukas!"

# Skripti lõpp
