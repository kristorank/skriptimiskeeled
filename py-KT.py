#!/usr/bin/python
# -*- coding: utf-8 -*-
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled python kontrolltöö

# Väljumiskoodid:
# 1 - Vale arv parameetreid
# 2 - Sisend ja väljund sama
# 3 - Sisendfaili lugemisel tekkis viga
# 4 - Tekkis yldine viga
# 5 - Väljundfaili lugemisel/loomisel tekkis viga

import sys  # Moodul parameetrite lugemiseks
from string import uppercase,lowercase,digits  # Moodul t2hem2rkide jadade saamiseks
from random import choice  # Moodul suvalise stringi genereerimiseks

epost = "@itcollege.ee"  # Muutuja olukorraks, kus e-postiaadressi vaja muuta
indeks = 0  # Muutuja massiivis navigeerimiseks

if len(sys.argv) == 3:  # Parameetreid on kokku kolm, skript ise kaasa arvatud
    sisendf = sys.argv[1]  # Sisendfaili parameeter
    print "Sisendfail: " + sisendf
    valjundf = sys.argv[2]  # Väljundfaili parameeter
    print "Väljundfail: " + valjundf

else:
    print "Vale arv parameetreid!"
    print "Kasutamine: " + sys.argv[0] + " <sisendfail> <väljundfail>"
    exit(1)

# Kontrollime, et skriptile ei anta mõlemaks parameetriks sama faili
if sisendf == valjundf:
    print "Sisend- ja väljundfailiks on antud sama fail!"
    print "Kasutamine: " + sys.argv[0] + " <sisendfail> <väljundfail>"
    exit(2)
    
try:
	with open(sisendf) as f:
		next(f)  # Ignoreerime p2ist
		
		# Loeme read massiivi, eraldades yksteisest andmed
		read = (rida.rstrip().split() for rida in f)  
		read = list(rida for rida in read if rida)  # Filtreerime v2lja tyhjad read
		
except IOError as e:  # Kui fail ei ole loetav, siis antakse viga
    print "Sisendfaili lugemisel tekkis viga!"
    exit(3)
    
except:
	print "Tekkis viga!"
	exit(4)

try:
	fail = open(valjundf, 'a')  # Avame väljundfaili kirjutamiseks
	
except IOError as e:  # Kui fail ei ole loetav, siis antakse viga
    print "V2ljundfaili avamisel tekkis viga!"
    exit(5)
    
except:
	print "Tekkis viga!"
	exit(4)


# Tsykkel v2ljundfaili kirjutamiseks
for line in read:

	# Salvestame eesnime muutujasse
	ees = read[indeks][1]
	
	# Salvestame perenime muutujasse
	pere = read[indeks][2]
	
	# Kirjutame v2ljundfaili kasutajanime (8 t2hem2rki, 1 ees + 7 pere)
	fail.write((ees[:1] + pere[:7]).lower() + ",")
	
	# Kirjutame v2ljundfaili ees- ja perenime
	fail.write(ees + " " + pere + ",")
	
	# Kirjutame v2ljnudfaili e-postiaaddressi
	fail.write((ees + "." + pere).lower() + epost +",")
	
	# Kirjutame v2ljundfaili 20 t2hem2rki suvalist (token)
	fail.write((''.join(choice(uppercase + lowercase + digits + '-_') for i in range(20))))
		
	fail.write("\n")  # Alustame uut rida	
	indeks += 1  # Suurendame indeksit yhe v6rra, et liikuda massiivis edasi
	
fail.close()  # Sulgeme v2ljundfaili

print "Skript l6petas edukalt t88!"

