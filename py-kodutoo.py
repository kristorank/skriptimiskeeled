#!/usr/bin/python
# -*- coding: utf-8 -*-
# Autor: Kristo Ränk, 2016

# Skriptimiskeeled 3. kodutöö (python)
# Skript loeb esimese parameetrina antud failist sisse URL-id ja nende järel
# olevad stringid. String ja URL eraldatakse, seejärel tehakse päring vastava
# URLi pihta. Vastuse lähtekoodist otsitakse URLi järel olnud stringi.
# Teise parameetrina antud faili kirjutatakse uuesti URL, otsitav string ja
# "JAH/EI" vastavalt sellele, kas otsitav string leiti või mitte. 

#Väljumiskoodid:
# 1 - Vale arv parameetreid
# 2 - Sisend ja väljund sama
# 3 - Sisendfaili lugemisel tekkis viga
# 4 - Väljundfaili lugemisel/loomisel tekkis viga
# IOError tekib ka teistel põhjustel. Nt. kui võrk on maas
# 5 - URL'i pärimisel tekkis viga
# ValueError ei pruugi tulla URL'i pärimisega

# v0.1 - Parameetrite kontroll. Sisendfaili lugemine massiivi ning URL'i ja
# stringi eraldamine. Stringi otsimine lähtekoodist.
# v0.2 - Väljundfaili avamine/loomine. Väljundfaili ridade kirjutamine.
# Sisend- ja väljundfaili võrdlemine.

import sys # Moodul parameetrite(args) lugemiseks
import urllib2 # Moodul URL'ide pärimiseks

# Kontrollime, kas skriptile antakse õige arv parameetreid
if len(sys.argv) == 3: # Parameetreid on kokku kolm, skript ise kaasa arvatud
    sisendf = sys.argv[1] # Sisendfaili parameeter
    print "Sisendfail: " + sisendf
    valjundf = sys.argv[2] # Väljundfaili parameeter
    print "Väljundfail: " + valjundf
else:
    print "Vale arv parameetreid!"
    print "Kasutamine: " + sys.argv[0] + " <sisendfail> <väljundfail>"
    exit(1)

# Kontrollime, et skriptile ei anta mõlemaks parameetriks sama faili
if sisendf == valjundf:
    print "Sisend- ja väljundfailiks on antud sama fail!"
    print "Kasutamine: " + sys.argv[0] + " <sisendfail> <väljundfail>"
    exit(2)
    
# Sisendfaili lugemine massiivi
try: # Kui fail on loetav, siis read kirjutatakse massiivi
    with open(sisendf) as f:
        # Read kirjutatakse massiiivi ilma newline'ta
        # Samuti eraldatakse URL ja string
        # Tekib kahemõõtmeline massiiv, kus iga rea esimene element on URL
        # ja teine element string. Nt. [[URL][string]]
        massiiv = [line.rstrip().split() for line in f]
except IOError as e: # Kui fail ei ole loetav, siis antakse viga
    print "Sisendfaili lugemisel tekkis viga!"
    exit(3)

# Stringi otsimine leheküljelt ja väljundfaili tulemuse kirjutamine
#try:
    fail = open(valjundf, 'a') # Avame väljundfaili kirjutamiseks
    indeks = 0 # Muutuja massiivis reakaupa liikumiseks
    for line in massiiv:
        # Väljundfaili kirjutatakse URL ja string
        fail.write(massiiv[indeks][0] + " " + massiiv[indeks][1])        
        # Pärime URL'i abil lehekülje
        p2ring = urllib2.urlopen(massiiv[indeks][0])
        # Salvestame lehekülje lähtekoodi muutujasse
        html = p2ring.read()
        # Otsime lähtekoodist stringi, kui ei leita, siis find() tagastab "-1"
        if str(html.find(massiiv[indeks][1])) != "-1":
            fail.write(" JAH\n")
        else:
            fail.write(" EI\n")
        print "Pärime URL'ilt: " + massiiv[indeks][0] + " stringi " + massiiv[indeks][1]
        # Suurendame indeksit ühe võrra, et võetaks järgmine rida
        indeks = indeks + 1            
    fail.close() # Väljundfaili sulgemine kui oleme lõpetanud
except IOError:
    print "Väljundfaili loomisel tekkis viga!"
    exit(4)
except ValueError:
    print "URL'i pärimisel tekkis viga!"
    print "Kontrollida, kas sisendfailis on iga URL'i ees http://"
    fail.write("\n")
    fail.close()
    exit(5)

print "Skript lõpetas edukalt töö!"
